﻿using UnityEngine;
using System.Collections;


public class MainMenuState :  MonoBehaviour, State
{
    private Texture texture;
    private GameObject plane;

	// Use this for initialization
	public void Start () {
        plane = GameObject.Find("Main_Menu_Plane");
        texture = Resources.Load("Textures/Scenes/Main_Menu_Start") as Texture;
        plane.renderer.material.mainTexture = texture;
	}
	
	// Update is called once per frame
	public void Update () {
        
        if (Input.GetKeyDown(KeyCode.DownArrow) && GameManager.Instance.gameState == GameState.STARTNEWGAME)
        {
            texture = Resources.Load("Textures/Scenes/Main_Menu_Controls") as Texture;
            plane.renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.CONTROLS;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && GameManager.Instance.gameState == GameState.CONTROLS)
        {
            texture = Resources.Load("Textures/Scenes/Main_Menu_Credits") as Texture;
            plane.renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.CREDITS;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) && GameManager.Instance.gameState == GameState.CREDITS)
        {
            texture = Resources.Load("Textures/Scenes/Main_Menu_Exit") as Texture;
            plane.renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.EXIT;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && GameManager.Instance.gameState == GameState.CONTROLS)
        {
            texture = Resources.Load("Textures/Scenes/Main_Menu_Start") as Texture;
            plane.renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.STARTNEWGAME;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && GameManager.Instance.gameState == GameState.CREDITS)
        {
            texture = Resources.Load("Textures/Scenes/Main_Menu_Controls") as Texture;
            plane.renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.CONTROLS;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && GameManager.Instance.gameState == GameState.EXIT)
        {
            texture = Resources.Load("Textures/Scenes/Main_Menu_Credits") as Texture;
            plane.renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.CREDITS;
        }


        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.STARTNEWGAME)
        {
            GameManager.Instance.gameState = GameState.RUNNING;
            //DontDestroyOnLoad(GameManager.Instance.gameObject);
            //DontDestroyOnLoad(GameObject.Find("SceneChanger"));
            //Application.LoadLevel("blackandwhite");
            SceneChanger.Instance.loadNewScene("blackandwhite");
        }
        else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.CONTROLS)
        {
            texture = Resources.Load("Textures/Scenes/Controlls_Scene") as Texture;
            plane.renderer.material.mainTexture = texture;
        }
        else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.CREDITS)
        {
            texture = Resources.Load("Textures/Scenes/Credit_Scene") as Texture;
            plane.renderer.material.mainTexture = texture;
        }
        else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.EXIT)
        {
            Application.Quit();
        }
	}
}
