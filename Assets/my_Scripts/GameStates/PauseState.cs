﻿using UnityEngine;
using System.Collections;

public class PauseState : MonoBehaviour, State {

    //private State state { get; private set; }
    private Texture texture;
    private GameObject plane;

	// Use this for initialization
    public void Start()
    {
        Debug.Log("Pause_Menu");
        plane = GameObject.Find("Pause_Plane");
        Debug.Log("GameObject = " + plane.ToString());

        texture = Resources.Load("Textures/Scenes/Pause_Menu_Resume") as Texture;
        Screen.lockCursor = true;
        Time.timeScale = 0;
        //renderer.material.mainTexture = texture;
	}
	
	// Update is called once per frame
    public void Update()
    {

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Screen.lockCursor = true;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (GameManager.Instance.gameState == GameState.RESUME)
            {
                texture = Resources.Load("Textures/Scenes/Pause_Menu_New_Game") as Texture;
                plane.renderer.material.mainTexture = texture;
                GameManager.Instance.gameState = GameState.STARTNEWGAME;
            }
            else if ((GameManager.Instance.gameState == GameState.STARTNEWGAME))
            {
                texture = Resources.Load("Textures/Scenes/Pause_Menu_Controls") as Texture;
                plane.renderer.material.mainTexture = texture;
                GameManager.Instance.gameState = GameState.CONTROLS;
            }

            else if (GameManager.Instance.gameState == GameState.CONTROLS)
            {
                texture = Resources.Load("Textures/Scenes/Pause_Menu_Quit") as Texture;
                plane.renderer.material.mainTexture = texture;
                GameManager.Instance.gameState = GameState.QUIT;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if ((GameManager.Instance.gameState == GameState.STARTNEWGAME))
            {
                texture = Resources.Load("Textures/Scenes/Pause_Menu_Resume") as Texture;
                plane.renderer.material.mainTexture = texture;
                GameManager.Instance.gameState = GameState.RESUME;
            }

            else if (GameManager.Instance.gameState == GameState.CONTROLS)
            {
                Debug.Log("CREDITS");
                texture = Resources.Load("Textures/Scenes/Pause_Menu_New_Game") as Texture;
                plane.renderer.material.mainTexture = texture;
                GameManager.Instance.gameState = GameState.STARTNEWGAME;
            }
            else if (GameManager.Instance.gameState == GameState.QUIT)
            {
                texture = Resources.Load("Textures/Scenes/Pause_Menu_Controls") as Texture;
                plane.renderer.material.mainTexture = texture;
                GameManager.Instance.gameState = GameState.CONTROLS;
            }
        }

        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.RESUME)
        {
            Time.timeScale = 1;
            Screen.lockCursor = false;
            SceneChanger.Instance.reEnableLastScene();
        }

        else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.STARTNEWGAME)
        {
            Time.timeScale = 1;
            Screen.lockCursor = false;
            //Application.LoadLevel("blackandwhite");
            GameManager.Instance.gameState = GameState.RUNNING;
            SceneChanger.Instance.loadNewScene("blackandWhite");
            //Destroy(gameObject);
        }
        else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.CONTROLS)
        {
            Time.timeScale = 0;
            Screen.lockCursor = true;
            texture = Resources.Load("Textures/Scenes/Controlls_Scene") as Texture;
            plane.renderer.material.mainTexture = texture;        
        }
        else if ((Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown("Jump")) && GameManager.Instance.gameState == GameState.QUIT)
        {
            Time.timeScale = 1;
            Screen.lockCursor = false;
            //texture = Resources.Load("Textures/Scenes/Main_Menu_Start") as Texture;
            //renderer.material.mainTexture = texture;
            GameManager.Instance.gameState = GameState.STARTNEWGAME;
            SceneChanger.Instance.loadNewScene("Main_Menu_Start");
            //Application.LoadLevel("Main_Menu_Start");
            //Destroy(gameObject);
        }
	}
}
