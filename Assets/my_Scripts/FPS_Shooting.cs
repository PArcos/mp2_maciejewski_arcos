﻿using UnityEngine;
using System.Collections;

public class FPS_Shooting : MonoBehaviour {
    public GameObject bullet_prefab;
    public float bulletSpeed = 20.0f;
    public GameObject debrisPrefab;
    //public GameObject fireEffect;
    public float range = 50f;
    public float damage = 50f;
    public string button = "Fire1";

    private GameObject objects;

	// Use this for initialization
	void Start () {
        this.camera.backgroundColor = Color.white;
        objects = GameObject.Find("Objects");
        //this.camera.SetReplacementShader(Resources.LoadAssetAtPath<Shader>("Assets/Materials/OverlayInvert.shader"), "RenderType");
  	}
	
	// Update is called once per frame

	void Update () {
        //if (Input.GetButton("Fire1"))
        if (Input.GetButtonDown(button))
        {
            Camera cam = gameObject.camera;
            GameObject bullet = (GameObject)Instantiate(bullet_prefab, cam.transform.position + cam.transform.forward, cam.transform.rotation);
            bullet.rigidbody.AddForce(cam.transform.forward * bulletSpeed, ForceMode.Impulse);

            bullet.transform.parent = objects.transform;

            Ray ray = new Ray(cam.transform.position, cam.transform.forward);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, range))
            {
                Vector3 hitPoint = hitInfo.point;

                if (debrisPrefab != null)
                {
                    Instantiate(debrisPrefab, hitPoint, Quaternion.identity);
                }
            }
        }
	}
}
