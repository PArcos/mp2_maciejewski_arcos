﻿using UnityEngine;
using System.Collections;


public enum GameState { NULLSTATE, MAINMENU, STARTNEWGAME, CONTROLS, CREDITS, QUIT, EXIT, RESUME, RESTART, RUNNING }

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public GameState gameState { get; set; }

    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        _instance.gameState = GameState.STARTNEWGAME;
        SceneChanger.Instance.loadNewScene("Main_Menu_Start");
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape) && _instance.gameState == GameState.RUNNING)
        {
            _instance.gameState = GameState.RESUME;
            SceneChanger.Instance.disableCurSceneAndLoadnewAdditive("PauseMenu_New_Game");
        }
    }

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
}