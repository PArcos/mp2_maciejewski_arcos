﻿using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

    static SceneChanger _instance;

    public static SceneChanger Instance { get { return _instance; } }

    GameObject disabledObjects;

    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    private SceneChanger() { }

    public void loadNewScene(string scene)
    {
        if (disabledObjects != null)
        {
            disabledObjects.SetActive(true);
            foreach (Transform tr in disabledObjects.GetComponentInChildren<Transform>())
            {
                Debug.Log("Destroy: " + tr.gameObject.name);
                Destroy(tr.gameObject);
            }
            Destroy(disabledObjects);
        }
        GameObject obj = GameObject.Find("Objects");
        if (obj != null)
        {
            foreach (Transform tr in obj.GetComponentInChildren<Transform>())
            {
                Debug.Log("Destroy: " + tr.gameObject.name);
                Destroy(tr.gameObject);
            }
            Destroy(obj);
        }
        Application.LoadLevelAdditive(scene);

        LightmapSettings.lightmaps = new LightmapData[0];
        Resources.UnloadUnusedAssets();
    }

    public void disableCurSceneAndLoadnewAdditive(string scene)
    {
        GameObject obj = GameObject.Find("Objects");
        if (obj != null)
        {
            obj.name = "DisabledObjects";
            disabledObjects = obj;
            obj.SetActive(false);
        }
        Application.LoadLevelAdditive(scene);
        LightmapSettings.lightmaps = new LightmapData[0];
        Resources.UnloadUnusedAssets();
    }

    public void reEnableLastScene()
    {
        GameObject obj = GameObject.Find("Objects");
        if (obj != null)
        {
            foreach (Transform tr in obj.GetComponentInChildren<Transform>())
            {
                Debug.Log("Destroy: " + tr.gameObject.name);
                Destroy(tr.gameObject);
            }
            Destroy(obj);
        }
        if (disabledObjects != null)
        {
            disabledObjects.name = "Objects";
            disabledObjects.SetActive(true);
            disabledObjects = null;
        }
        LightmapSettings.lightmaps = new LightmapData[0];
        Resources.UnloadUnusedAssets();
        GameManager.Instance.gameState = GameState.RUNNING;
    }
}
