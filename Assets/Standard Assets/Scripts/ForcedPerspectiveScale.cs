﻿using UnityEngine;
using System.Collections;
using System;

public class ForcedPerspectiveScale : MonoBehaviour {

    Vector3 initialPos, finalPos;
    Collider dragged;
    float scaleFactor = 0.1f;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (Input.GetButtonDown("Fire1"))
        {

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            ray.origin = transform.position;

          
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500.0f))
            {
                initialPos = hit.point;
                dragged = hit.collider;
            }
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            ray.origin = transform.position;


            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500.0f))
            {
                finalPos = hit.point;
                float distance = Vector3.Magnitude(finalPos - initialPos);
                Debug.Log(dragged);
                dragged.transform.localScale *= distance * scaleFactor;
            }
        }
	}
}
