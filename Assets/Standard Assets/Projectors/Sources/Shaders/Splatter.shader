﻿Shader "Splatter Shader" {
   Properties {
      _SplatterTex ("Cookie", 2D) = "gray" { }
	  _ShadowMap ("RenderTexture", 2D) = "white" { }
   }
   SubShader {

      Pass {
		 ZWrite On
		 Blend SrcAlpha OneMinusSrcAlpha

         CGPROGRAM
 
         #pragma vertex vert  
         #pragma fragment frag 
 
         // User-specified properties
         uniform sampler2D _SplatterTex;
		 uniform sampler2D _ShadowMap; 
 
         // Projector-specific uniforms
         uniform float4x4 _Projector; // transformation matrix 
            // from object space to projector space 
 
          struct vertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 posProj : TEXCOORD0;
         };
 
         vertexOutput vert(vertexInput input) 
         {
            vertexOutput output;
 
            output.posProj = mul(_Projector, input.vertex);
            output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
            return output;
         }
			 
         float4 frag(vertexOutput input) : COLOR
         {
			float shadow = tex2Dproj(_ShadowMap, input.posProj).r;
            if(input.posProj.w > 0 && shadow - 0.07 > input.posProj.z / input.posProj.w) // in front of projector?
            {
            	//return float4(0, 0, 0, 1);
				float4 color = tex2Dproj(_SplatterTex, input.posProj);
				return color;
            }
			else
			{
				return float4(1, 1 , 1, 0);
			}

         }
 
         ENDCG
      }
   }  
   // The definition of a fallback shader should be commented out 
   // during development:
   // Fallback "Projector/Multiply"
}