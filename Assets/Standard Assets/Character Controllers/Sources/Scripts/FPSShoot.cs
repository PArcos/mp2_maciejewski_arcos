﻿using UnityEngine;
using System.Collections;

public class FPSShoot : MonoBehaviour {
    public GameObject Splatter;
    public Shader DepthWrite;
    
    public AudioClip[] sounds;
    public Texture[] textures;

	// Use this for initialization
	void Start () {
	}
	
    void OnCollisionEnter(Collision col)
    {
        Vector3 dir = -col.contacts[0].normal;
        Vector3 splatCast = col.contacts[0].point - (-col.contacts[0].normal * 4f);
        
        Texture splatterTex = textures[Random.Range(0, textures.Length)];


        CreateSplatter(splatCast, Quaternion.LookRotation(dir), (1 << LayerMask.NameToLayer("Player")), splatterTex);

        if (col.collider.gameObject.tag.Equals("Player"))
        {
            GameObject playerSplatter = CreateSplatter(splatCast, Quaternion.LookRotation(dir), ~(1 << LayerMask.NameToLayer("Player")), splatterTex);
                        
            playerSplatter.transform.parent = col.transform;

            // Calculate damage
            float damage = CalculateHitPercentage(col.collider.bounds, playerSplatter.camera);

            var hasHealth = col.gameObject.GetComponent<HasHealth>();
            if(hasHealth) hasHealth.receiveDamage(damage);
        }

        // Play sound
        AudioSource.PlayClipAtPoint(sounds[Random.Range(0, sounds.Length)], transform.position);

        Destroy(gameObject);
    }

	// Update is called once per frame
	void Update () {
	}

    private GameObject CreateSplatter(Vector3 pos, Quaternion rot, int ignoredLayer, Texture splatterTex)
    {
        GameObject splatterInstance = Instantiate(Splatter, pos, rot) as GameObject;

        splatterInstance.camera.depthTextureMode = DepthTextureMode.Depth;

        float aspect = splatterInstance.camera.aspect;
        splatterInstance.camera.targetTexture = new RenderTexture((int)(64.0f * aspect), 64, 16, RenderTextureFormat.Default);
        splatterInstance.camera.SetReplacementShader(DepthWrite, "");
        splatterInstance.camera.Render();
        splatterInstance.camera.enabled = false;

        // Set shadow map in shader
        Projector projector = splatterInstance.GetComponent<Projector>();
        projector.aspectRatio = aspect;
        
        projector.material = new Material(projector.material);
        projector.material.SetTexture("_SplatterTex", splatterTex);
        projector.material.SetTexture("_ShadowMap", splatterInstance.camera.targetTexture);
        projector.ignoreLayers = ignoredLayer;

        return splatterInstance;
    }



    private float CalculateHitPercentage(Bounds bounds, Camera camera) 
    {
        float minX = Mathf.Infinity;
        float minY = Mathf.Infinity;
        float maxX = -Mathf.Infinity;
        float maxY = -Mathf.Infinity;
 
        Vector3 c = bounds.center;
        Vector3 e = bounds.extents;
 
        Vector3[] corners = new Vector3[8];
        corners[0]  = new Vector3(c.x - e.x, c.y + e.y, c.z - e.z);  // Front top left corner
        corners[1]  = new Vector3(c.x + e.x, c.y + e.y, c.z - e.z);  // Front top right corner
        corners[2]  = new Vector3(c.x - e.x, c.y - e.y, c.z - e.z);  // Front bottom left corner
        corners[3]  = new Vector3(c.x + e.x, c.y - e.y, c.z - e.z);  // Front bottom right corner
        corners[4]  = new Vector3(c.x - e.x, c.y + e.y, c.z + e.z);  // Back top left corner
        corners[5]  = new Vector3(c.x + e.x, c.y + e.y, c.z + e.z);  // Back top right corner
        corners[6]  = new Vector3(c.x - e.x, c.y - e.y, c.z + e.z);  // Back bottom left corner
        corners[7]  = new Vector3(c.x + e.x, c.y - e.y, c.z + e.z);  // Back bottom right corner
 
        // Calculate corners 
        foreach(Vector3 corner in corners) {
            Vector3 viewPoint = camera.WorldToViewportPoint(corner);
            if (viewPoint.x > maxX) maxX = viewPoint.x;
            if (viewPoint.x < minX) minX = viewPoint.x;
            if (viewPoint.y > maxY) maxY = viewPoint.y;
            if (viewPoint.y < minY) minY = viewPoint.y;
        }
 
        float width = maxX - minX;
        float height = maxY - minY;
        float area = width * height;
        float percentage = area / (camera.pixelWidth * camera.pixelHeight) * 100000.0f;

        return percentage;
        
    }
}

