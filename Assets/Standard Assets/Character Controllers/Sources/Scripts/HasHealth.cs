﻿using UnityEngine;
using System.Collections;

public class HasHealth : MonoBehaviour {

    public float health = 100f;

    void Start()
    {

    }

    void Update()
    {
    }

    public void receiveDamage(float dam)
    {
        health -= dam;
        Debug.Log("health = " + health);

        if (health <= 0)
        {
            Debug.Log(gameObject.name + " died!");
            Destroy(gameObject);
        }
    }

}
